﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace ReverseInParantheses
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = "(bar)";
            string s2 = "foo(bar)baz";
            string s3 = "foo(bar)baz(blim)";
            string s4 = "foo(bar(baz))blim";
            string s5 = "";
            string s6 = "()";
            string s7 = "(abc)d(efg)";

            string a1 = reverseInParentheses(s1);
            string a2 = reverseInParentheses(s2);
            string a3 = reverseInParentheses(s3);
            string a4 = reverseInParentheses(s4);
            string a5 = reverseInParentheses(s5);
            string a6 = reverseInParentheses(s6);
            string a7 = reverseInParentheses(s7);

            System.Console.WriteLine(a1 + " " + a2 + " " +
            a3 + " " +
            a4 + " " +
            a5 + " " +
            a6 + " " +
            a7 + " ");



        }
        
        // static string reverseInParentheses(string inputString) {
        //     if(inputString.Contains('(')){
        //         string subString = inputString.Substring(inputString.IndexOf('(') + 1, inputString.LastIndexOf(')') - (inputString.IndexOf('(') + 1 ));
        //         return subString;
        //     }
        //     return "";
        // }

        static string reverseInParentheses(string inputString) {
            // while(inputString.Contains("("))
            // {
            //     int i = inputString.LastIndexOf("(");
            //     var s = new string(inputString.Skip(i + 1).TakeWhile(x => x != ')').Reverse().ToArray());
            //     var t = "(" + new string(s.Reverse().ToArray()) + ")";
            //     inputString = inputString.Replace(t, s);
            // }
            // return inputString;
            if(!inputString.Contains('(')){
                char[] charArray = inputString.ToCharArray();
                Array.Reverse(charArray);
                return new String(charArray);
            } 
            
            // extract the words inside the parenthsis
            var found = 0;
            var wordToReverse = "";
            var exit = -1;
            var enter = -1;
            for (int i = 0 ; i< inputString.Length; i++)
            {
                var item = inputString[i];
                if (found > 0) {
                    if (item == '(') {
                        found++;
                        wordToReverse = "";
                        enter = i;
                    } else if (item == ')') {
                        found--;
                        exit = i;
                        break;
                    } else {
                        wordToReverse += item;
                    }
                } else {
                    if (item == '(') {
                        found++;
                        enter = i;
                    }
                }
            }

            // reverse them
            var reversedWord = reverseInParentheses(wordToReverse);

            // add them back into the original string
            var endOfWord = "";
            if (exit < inputString.Length -1) {
                endOfWord = inputString.Substring(exit + 1, inputString.Length - exit - 1);
            }
            var wordToReturn = inputString.Substring(0, enter) + reversedWord + endOfWord;
            if (wordToReturn.Contains('(')) {
                return reverseInParentheses(wordToReturn);
            } else {
                return wordToReturn;
            }
        }
    }
}